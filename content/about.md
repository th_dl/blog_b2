---
draft: false
title: "À propos"
---

Vous trouverez sur ce blog différents articles concernant mon stage de fin de deuxième année que j'ai effectué
chez _Veolia SETOM_.

## Remerciements

Je tiens tout d'abord à remercier mon maître de stage _Arnaud Dupuis_ qui m'a guidé tout au long de ce projet, et sans
qui ce stage n'aurait pas été possible.

Merci également à _Jérôme Natta_, le directeur de la SETOM, à Maris-Anaïs Londres et Valérie Loubet, des resources
humaines, ainsi qu'à l'ensemble des équipes de la SETOM pour leur accueil chaleureux.

Pour finir, j'aimerais remercier tout particulièrement mes collègues de l'équipe IT _Claude Madeleine_, _Jean Douet_ et
_Frédéric Albiztur_, qui m'ont plusieurs fois dépanné et avec qui j'ai passé de très bons moments.

---

Ce blog a été réalisé à l'aide d'[Hugo](https://gohugo.io) (un générateur de site web statique), avec le
thème [Hello Friend](https://themes.gohugo.io/themes/hugo-theme-hello-friend/).

---

#### Retrouvez toutes les informations pour me joindre sur la page [Contact]({{< ref "/contact" >}})
