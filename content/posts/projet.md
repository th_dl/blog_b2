---
draft: false
title: "Le projet"
date: 2022-07-09T14:39:53+02:00
author: Thomas
tags: ["stage"]
description: "Problématique, explication du projet et choix des technologies"
---

## Problématique

Comme nous l'avons vu dans la partie concernant
[le traitement de l'eau]({{< ref "/presentation-setom#le-traitement-de-leau" >}}), l'obtention d'une eau de qualité
nécessite un processus assez complexe.
La composition de l'eau brute (l'eau encore non traitée) pouvant varier selon plusieurs facteurs (température, pluie,
intervention en amont du point de captage, ...), il est nécessaire de mesurer régulièrement différentes propriétés de
celle-ci, telles que son pH ou sa turbidité.

Pendant environ 20 ans, les relevés ont été enregistrés dans une base de données _Microsoft Access_, puis durant
quelques années sur des feuilles _Microsoft Excel_. Désormais, en raison d'une volonté de centraliser et de sécuriser
ces données ainsi que de faciliter leur saisie, le développement d'une application dédiée s'est révélé être un choix
cohérent vis-à-vis de ces différents besoins.

## Détail du projet

La mission énoncée ci-dessus constitue en fait 2 tâches bien distinctes :

1. La reprise des données _Access_ et _Excel_ et leur stockage dans une base de données _PostgreSQL_
2. L'affichage des relevés déjà effectués et la saisie de nouveaux

#### Reprise des données

Cette partie ayant été conçue par mon prédécesseur, je vais seulement expliquer son fonctionnement dans les grandes
lignes.

Il s'agit d'un programme **Java** qui définit les modèles de données via l'ORM [Hibernate](https://hibernate.org/orm/),
crée les tables correspondantes dans la base de données, puis extrait les données contenues dans les fichiers _Access_
et _Excel_ et les insère dans les tables.

J'ai quand même dû apporter quelques modifications à ce programme, notamment pour ajouter la capacité
d'auto-incrémentation à certaines tables, ainsi que pour régler des problèmes d'encodage lors de l'import des données
venant d'_Access_.

![Schéma de la base de données Postgres](img/db_schema.svg)

#### Interface d'affichage et de saisie

Une interface en **PHP** avait également été commencée par mon prédécesseur, mais elle n'était que peu avancée et la
façon dont elle avait été conçue ne permettait aucune flexibilité, notamment quant à l'ajout, la modification ou le
retrait des champs de saisie des données.

Avec mon maître de stage _Arnaud Dupuis_, nous avons donc décidé de recommencer à zéro cette interface et j'ai proposé
d'utiliser le langage **Go** pour sa réalisation.
En effet, fort de ses nombreux avantages tels que le fait d'être performant, concurrent, avec un typage statique et sûr,
le **Go** s'est avéré être une technologie pertinente pour la réalisation de l'interface web.
Du fait de son ample modularité, ce langage permet tout aussi bien de manipuler des requêtes HTTP que de mettre à jour
une base de données, ce qui va ainsi nous permettre de réaliser une application full-stack de type
[MVC]({{< ref "/structure#introduction" >}}).

Les principales fonctionnalités de cette application seront :

- Une authentification via le service de **SSO** de _Google_
- Un système de visualisation des relevés et des mesures
- Un formulaire de création et de modification de relevés
