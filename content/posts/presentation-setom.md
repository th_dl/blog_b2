---
draft: false
title: "Présentation de la SETOM"
date: 2022-07-02T14:50:19+02:00
author: Thomas
tags: ["stage"]
description: "Découvrez les enjeux de la gestion de l'eau dans la métropole toulousaine"
cover: "img/SETOM_Flourens.png"
coverCaption: "Les locaux de la SETOM à Flourens"
---

## Contexte

Depuis le 1er janvier 2020 et pour une durée de 12 ans, la gestion de l'eau de Toulouse Métropole est opérée par deux
sociétés :

- SETOM, une filiale de Veolia, s'occupe de l'eau potable
- ASTEO, une filiale de Suez, de l'assainissement des eaux usées

Ces deux entités opèrent conjointement sous le nom d'[Eau de Toulouse Métropole](https://www.eaudetoulousemetropole.fr).

La zone couverte par les services d'Eau de Toulouse Métropole s'étend en fait extra-muros et dessert à l'heure actuelle
27 communes.

![Communes desservies par Eau de Toulouse Métropole - Crédit image EaudeTM](img/communes.svg)

## Le rôle de la SETOM

L'approvisionnement en eau potable des différentes communes s'effectue en plusieurs étapes.

### Le prélèvement de l'eau

L'eau destinée à être consommée dans l'agglomération toulousaine provient principalement de 2 endroits :

- La Garonne/l'Ariège à 97%
- La Montagne Noire à 3%

Elle est alors pompée et acheminée vers des usines de traitement des eaux.

### Le traitement de l'eau

Afin de rendre l'eau propre à la consommation, cette dernière passe par des usines qui ont pour but d'enlever toutes les
impuretés, ainsi que de la traiter chimiquement.

Il existe 3 de ces usines, se situant à :

- Pech-David
- Clairfont
- Tournefeuille

Voici maintenant les différentes étapes nécessaires au traitement de l'eau :

1. La floculation
2. La décantation
3. La filtration
4. La désinfection à l'ozone

Le traitement de l'eau est très surveillé et des relevés sont effectués plusieurs fois par jour afin d'en vérifier la
conformité.

### La distribution de l'eau

Une fois traitée, l'eau est envoyée dans des châteaux d'eau afin d'être stockée en hauteur en attendant d'être
distribuée dans le réseau.
La SETOM s'occupe donc également du raccordement au réseau d'eau potable ainsi que des compteurs.
