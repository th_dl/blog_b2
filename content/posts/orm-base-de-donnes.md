---
draft: false
title: "ORM & base de données"
date: 2022-07-23T14:35:59+02:00
author: Thomas
tags: ["stage", "golang", "base de données"]
description: "Le fonctionnement de la relation entre la base de données, la logique et l'interface"
---

## Introduction

Les interactions avec la base de données représentent leur cœur de l'application. En effet, toute action effectuée
depuis l'interface web implique soit une lecture, soit une écriture en base. Il était donc nécessaire de mettre en place
une solution facile d'utilisation et relativement optimisée : passer par un ORM (Object Relational Mapping), autrement
dit une librairie qui se charge de faire le lien pour nous entre la base de données et les objets dans notre code.

## Gorm

[Gorm](https://gorm.io/) est un ORM open-source exclusivement fait en **Go**. Du fait de son nombre important de
fonctionnalités, de la fréquence de ses mises à jour et de sa [documentation](https://gorm.io/docs/index.html) des plus
complètes, c'est lui que nous avons choisi pour ce projet.

### La déclaration des modèles

La déclaration des modèles se fait en plusieurs étapes. Prenons l'exemple du modèle de relevé, `Statement` :

{{< code language="go" title="database/models/statement.go" >}}
type Statement struct {
    Id int                `gorm:"primaryKey"`
    Date time.Time
    Numero int
    Observation string
    OperateurId int
    Operateur Operator    `gorm:"foreignKey:OperateurId"`
    UsineId int
    Usine Factory         `gorm:"foreignKey:UsineId"`
    // RelatedMeasures contains the id of each Measure that belongs to this Statement
    RelatedMeasures []int `gorm:"-"`
}
{{< / code >}}

Tout d'abord, étant donné que le nom du type **Go** est en anglais mais que le nom de la table (_relevé_) est en
français, on doit indiquer à **Gorm** le bon nom de la table, car ce dernier va essayer de déduire le nom de la table en
fonction du nom du type **Go** via un travail de
[flexion grammaticale](https://fr.wikipedia.org/wiki/Flexion_(linguistique)) (`Statement` => _statements_).

On peut donc implémenter l'interface [^1] `schema.Tabler`, grâce à la méthode `TableName` dans laquelle nous retournons
le
bon nom de table.

Ensuite, dans l'extrait de code ci-dessus, on peut remarquer deux choses :

- Les _tags_, ces instructions entre backticks (`) après :
    - le champ _Id_, la clé primaire de la table _relevé_
    - les champs _Operateur_ et _Usine_, qui sont des clés étrangères
    - le champ _RelatedMeasures_, où le but est d'indiquer à **Gorm** de l'ignorer
- Les susdites clés étrangères sont doublées d'un autre champ suffixé de "Id".

Les _tags_ commençant par `gorm:"foreignKey:` indiquent que le champ qui les précède représente une clé étrangère sur le
champ renseigné dans le tag (exemple: ```Usine Factory `gorm:"foreignKey:UsineId"` ``` => `UsineId int`).

La table _relevé_ ne va donc pas contenir le champ `Usine`, mais seulement le champ `UsineId`. Et comme l'usine
référencée dans cette table n'y est pas stockée, lorsque l'on remonte un relevé de la base de données il faut aussi
remonter l'usine correspondante. C'est pour ça que **Gorm** a mis a place des _hooks_, c'est-à-dire des méthodes
appelées automatiquement à certains moments.

#### l'AfterFind

Le _hook_ `AfterFind` est une méthode définie sur les objets que l'on souhaite stocker en base de données appelée comme
son nom l'indique lorsque l'objet a été trouvé. Nous avons donc implémenté ce _hook_ pour compléter certaines propriétés
de notre objet, telles que `Operateur`, `Usine` et `RelatedMeasures`.

{{< code language="go" title="database/models/statement.go" >}}
func (statement *Statement) AfterFind(db *gorm.DB) error {
    var errOp error
    if opId := statement.OperateurId; opId > 0 {
        // retrieving related operator
        errOp = db.Where(Operator{Id: opId}).Find(&statement.Operateur).Error
    }
    return mergeErrors(
        errOp,
        // retrieving related factory
        db.Where(Factory{Id: statement.UsineId}).Find(&statement.Usine).Error,
        // retrieving related measures
        db.Select("id").Where(Measure{ReleveId: statement.Id}).Find(&statement.RelatedMeasures).Error,
    )
}
{{< / code >}}

On constate effectivement dans cet extrait de code 3 requêtes à la base de données dans l'objectif de récupérer ces
propriétés.

Cependant, cette solution ne marche pas dans tous les cas.

Dans le module database sont déclarées des fonctions ayant pour but d'interagir avec la base de données de façon
générique. Prennons l'exemple de la fonction `FindModels`, qui a pour but de trouver les objets du type donné avec
certains critères, et dont voici la signature :

```go
func FindModels(model models.Model, maxRows, targetId int, page *int, order string, conditions map[string]any) any
```

Le premier paramètre est le type du modèle que l'on souhaite récupérer, et `models.Model` est une interface implémentée
par tous nos modèles.
Le type de retour de cette fonction est donné comme pouvant être n'importe quoi (`any`), mais sera en fait toujours
une _slice_ [^2] du type de modèle fourni. La raison est que cette _slice_ est fabriquée dynamiquement via de la
_réflection_ (métaprogrammation), et le type de retour varie donc en fonction du modèle donné.

Et c'est avec toutes ces couches d'abstraction au-dessus du modèle de base (comme un objet `Statement` ou `Operator`)
que le hook _AfterFind_ ne marche plus pour la simple et bonne raison que les éléments de cette _slice_ ne sont plus vus
comme implémentant l'interface.

La solution à ce problème a été de faire appel une nouvelle fois à la réflection, et de 'creuser' dans la valeur
retournée par la fonction pour réellement savoir si oui ou non le modèle donné implémente l'interface _AfterFind_.

{{< code language="go" title="database/utils.go" >}}
func afterFind(models any) any {
    s := reflect.ValueOf(models)
    if s.Kind() != reflect.Ptr {
        logger.Errorf("Can't afterFind: %T should be a pointer", models)
        return models
    }
    if s.Elem().Elem().Kind() != reflect.Slice {
        logger.Errorf("Can't afterFind: %T should be a pointer to a slice", models)
        return models
    }
    for i := 0; i < s.Elem().Elem().Len(); i++ {
        e := s.Elem().Elem().Index(i).Interface()
        if v, ok := e.(callbacks.AfterFindInterface); ok {
            err := v.AfterFind(database)
            if err != nil {
                logger.Warnf("Failed to afterFind %T: %v", e, err)
                continue
            }
        }
    }
    return s.Elem().Interface()
}
{{< / code >}}

On peut décomposer la fonction ci-dessus en plusieurs étapes :

1. (`s.Kind() != reflect.Ptr`) Vérification que le paramètre `models` est bien un pointeur
2. (`s.Elem().Elem().Kind() != reflect.Slice`) Vérification que la valeur référencée par `models` est bien une _slice_
3. (`for i := 0; i < s.Elem().Elem().Len(); i++`) Itération autour de la _slice_ référencée par `models`
4. (`e := s.Elem().Elem().Index(i).Interface()`) La récupération de l'élément à l'index `i` de cette _slice_
5. (`v, ok := e.(callbacks.AfterFindInterface); ok`) La vérification de si l'élément implémente bien l'interface
   _AfterFind_ grâce à un _cast_
6. (`err := v.AfterFind(database)`) L'appel de la méthode _AfterFind_ sur l'élément qui l'implémente donc

Grâce à cette démarche, tous les modèles de la _slice_ ont pu être complétés via le hook _AfterFind_.

#### Les scopes

Le volume de données manipulées étant assez important (3 relevés par jour sur 3 usines pendant plus de 20 ans, chaque
relevé pouvant être composé d'une cinquantaine de mesures), il était nécessaire de mettre en place un système de
pagination
sur l'interface web. **Gorm** n'implémente pas directement un système de pagination, mais on peut utiliser ses fonctions
pour en faire l'équivalent.

Via la méthode `Scopes`, on peut limiter notre sélection dans la base de données via des conditions, notamment
d'_offset_ et de limites dans notre cas.

En effet, après avoir récupéré la page demandée par l'utilisateur ainsi que le nombre d'éléments à afficher par page, on
peut exécuter la logique ci-dessous, qui trouve sa place dans la fonction `FindModels` (explicitée un peu plus haut) :

{{< code language="go" title="database/functions.go" >}}
// evaluating the maximum page available for the given model according to the given conditions
modelsCount := Count(model, conditions)
lastPage := int(math.Ceil(float64(modelsCount) / float64(maxRows)))
// ensuring that the requested page is not greater than the last page
if *page > lastPage {
    *page = lastPage
} else if *page < 1 {
    *page = 1
}
// evaluating the number of rows to skip, depending on the number of pages & items per page
offset := (*page - 1) * maxRows

database.Scopes(func(db *gorm.DB) *gorm.DB {
    return db.Offset(offset).Limit(maxRows)
})
{{< / code >}}

La [transaction](https://fr.wikipedia.org/wiki/Transaction_informatique) retournée sera donc limitée aux données
contenues dans la plage définie entre `offset` et `offset + maxRows`.

[^1]: Une interface est un type abstrait qui garantit que le type de la valeur qu'elle masque implémente bien certaines
méthodes, et qu'il peut donc être considéré en tant que telle.
[^2]: La _slice_ est un type de base du **Go**, on peut la voir comme un _array_ à taille dynamique.
