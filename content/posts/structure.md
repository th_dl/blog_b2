---
draft: false
title: "Structure"
date: 2022-07-16T11:48:17+02:00
author: Thomas
tags: ["stage", "golang"]
description: "Détail de l'architecture du projet et des différents modules sur lesquels il repose"
---

## Introduction

L'application à développer étant basée sur le motif d'architecture logicielle **MVC** (Modèle/Vue/Contrôleur), sa
structure est donc principalement composée de ces trois fonctionnalités. En effet, nous allons retrouver un module
dédié aux objets qui serviront à représenter les données stockées en base de données, un autre contenant les différentes
vues de notre site web et un dernier rassemblant les contrôleurs, c'est-à-dire les fonctions prenant en charge les
requêtes HTTP.
Nous allons également trouver d'autres modules tels que celui gérant la partie **i18n** (**i**nternationalisatio**n**),
mais aussi des modules utilitaires contenant des fonctions génériques, ou bien encore un module dédié à la configuration
et à la validation des règles métier.

Voici maintenant une vue d'ensemble de l'arborescence des différents dossiers qui composent le projet :

```
.
└── src
    ├── constants
    ├── database
    │   └── models
    ├── factorules
    ├── i18n
    │   └── locales
    ├── logger
    ├── resources
    │   ├── assets
    │   │   ├── css
    │   │   ├── fonts
    │   │   ├── img
    │   │   └── js
    │   └── views
    │       └── components
    ├── users
    ├── utils
    └── web
        ├── authentication
        └── respmodels
```

## src

C'est le dossier qui contient toutes les sources du projet, et donc l'ensemble des différents modules qui le composent.

```
src
├── main.go
├── routes.go
├── viper.go
├── constants
├── database
├── factorules
├── i18n
├── logger
├── resources
├── users
├── utils
└── web
```

On y retrouve également quelques fichiers **Go** :

- `main.go` : le point d'entrée de l'application, qui va en quelque sorte démarrer les autres modules
- `viper.go` : là où sont traitées toutes les sources de configurations (fichier de configuration, arguments de ligne de
  commande, variables d'environnement)
- `routes.go` : là où sont déclarées toutes les routes de l'application ainsi que les méthodes qui les prennent en
  charge (GET, POST, ...)

### constants

Ce module contient un seul fichier, regroupant quelques constantes accessibles depuis l'ensemble du projet, comme par
exemple :

- la version du programe
- le nombre de lignes des tables à afficher par défaut

### database

Comme son nom l'indique, ce package sert d'interface à la base de données.

```
database/
├── conditions.go
├── config.go
├── database.go
├── functions.go
├── utils.go
└── models
```

Le fichier `config.go` contient la configuration d'accès à la base de données (adresse, nom d'utilisateur, mot de passe,
...).

`database.go` sert à l'initialisation de la connexion à la base de données.

`conditions.go` regroupe des fonctions pouvant générer des conditions **SQL**, comme par exemple pour ne sélectionner
que les relevés effectués par un certain opérateur.

`functions.go` dispose de fonctions d'accès à la base de données, comme pour récupérer d'un coup toutes les usines ou
encore pour trouver tous les modèles d'un certain type.

`utils.go` recense quelques fonctions utiles au bon fonctionnement des autres fichiers ci-dessus.

#### models

Les modèles servent à représenter les divers objets stockés en base de données.

```
database/models/
├── association-type.go
├── factory.go
├── measure.go
├── measure-type.go
├── model.go
├── operator.go
├── reference.go
├── statement.go
├── utils.go
├── utils_test.go
└── water-type.go
```

On peut donc retrouver les types d'associations, de mesures et d'eaux, les usines, les opérateurs, les mesures et bien
sûr les relevés.

On trouve également le fichier `model.go` qui contient une interface implémentée par tous les modèles, de façon à
obtenir une généricité dans toute l'application.

Le fichier `reference.go`, lui, permet de gérer les relations entre les différents modèles notamment lors de leur
affichage.

Pour finir, le fichier `utils.go` contient encore une fois des fonctions utilitaires et son fichier de test
`utils_test.go` permet quant à lui de vérifier son bon fonctionnement.

### factorules

Le module factorules - dont le nom est un mot-valise composé de *factory* et de *rules* - est consacré à la prise en
charge des règles métiers.

Les règles métiers sont ce qui définit les différentes étapes de traitement de l'eau et chaque propriété à mesurer,
vis-à-vis de l'application. Grâce à plusieurs fichiers **Json** (dont nous reparlerons plus tard) décrivant chacun la
structure d'une usine, on peut ensuite créer dynamiquement un formulaire recensant toutes les propriétés à mesurer à
chaque étape du traitement de l'eau, et ce, pour chaque usine.

```
factorules/
├── db-loader.go
├── factory-rules.go
├── field-props.go
├── loader.go
├── parser.go
├── section-field.go
├── section.go
├── standalone-table.go
├── tranche.go
└── validation.go
```

Le fichier `factory-rules.go` contient un objet décrivant la racine de la structure d'une usine, et notamment la liste
des sections (étapes du traitement de l'eau) qui la composent et qui sont stockés dans le fichier `section.go`. Ce
dernier rassemble toutes les propriétés à mesurer de cette étape, stockées dans les fichiers `section-field.go`,
`tranche.go`, `field-props.go` et `standalone-table.go`.

Les fichiers `loader.go` et `parser.go` servent eux à transformer les fichiers **Json** en objets décrits par les
fichiers cités ci-dessus.

`validation.go` s'occupe de valider les données entrées dans les formulaires et de les stocker en base de données.

Enfin, `db-loader.go` permet de charger les données depuis la base de données dans le but de les afficher ou de les
modifier.

### i18n

L'internationalisation (abrégée en **i**18**n** en raison de son nombre de lettres) permet comme son nom l'indique de
traduire le texte généré par l'application dans la langue de l'utilisateur.

Pour ce faire, la langue demandée doit tout de même être prédéclarée dans un fichier de langue, appelé *locale*.

```
i18n/
├── i18n.go
├── utils.go
├── utils_test.go
└── locales
    └── locale.fr.yml
```

C'est donc sans surprise que l'on retrouve un dossier `locales` qui regroupe toutes les langues disponibles à
l'internationalisation. En l'occurrence, seulement le français est défini.

Le fichier `i18n.go` sert à l'initialisation du module, au chargement des locales et déclare les fonctions utilisées
pour internationaliser les messages voulus.

`utils.go` contient encore une fois des fonctions utilitaires et son fichier de test `utils_test.go` sert à veiller à
son bon fonctionnement.

### logger

Le logger constitue une partie assez importante du projet, car il permet de connaître à tout moment le bon déroulement
de ce dernier.

```
logger/
└── logger.go
```

On ne trouve dans ce module qu'un seul fichier, mais il sert à la fois à son initialisation et à la déclaration des
fonctions permettant la journalisation à tous les niveaux.

En effet, plusieurs niveaux de *logging* sont disponibles :

- debug (tous les messages possibles)
- info (tous les messages sauf ceux à but de debug)
- warn (seulement les messages relativements importants et au-dessus)
- error (seulement les messages très importants)
- fatal (seulement les messages de crash)

Par ailleurs, la journalisation peut s'afficher de plusieurs manières : en **Json** ou non, en couleur ou non, dans un
fichier ou non et un système de _rolling logger_ est implémenté, c'est-à-dire que dès lors qu'un fichier de logs atteint
une certaine taille, un autre fichier est créé et le premier fichier peut être compressé.

### resources

Grâce à la fonctionnalité *embed* de **Go** introduite en version 1.16, il est possible de stocker le contenu de
fichiers et de dossiers dans des variables.
C'est d'ailleurs ce à quoi le package *resources* est dédié, car on y retrouve bon nombre de fichiers auxquels il sera
indispensable d'accéder lors de l'exécution du programme.

```
resources/
├── factory_statement_rules.schema.json
├── resources.go
├── assets
└── views
```

Tout d'abord, le fichier `resources.go` déclare l'ensemble des variables décrites ci-dessus. Elles le sont de telle
façon à ce qu'elles soient accessibles à l'ensemble du projet.

Ensuite, le fichier `factory_statement_rules.schema.json` contient un [schéma Json](https://json-schema.org/)
représentant la structure à laquelle doivent se plier les fichiers *factory rules* pour être considérés comme valides.

#### assets

Dans ce dossier, on trouve plusieurs sous-dossiers qui ont tous pour but d'être des ressources accessibles depuis
l'interface web.

```
resources/assets/
├── css
├── fonts
├── img
└── js
```

Le dossier `css` contient les feuilles de styles du framework **Bootstrap** ainsi que celles créées spécialement pour ce
projet, tout comme le dossier `js` qui contient des fichiers **JavaScript** dans les mêmes proportions.

Le dossier `fonts` sert à stocker les polices d'écriture utilisées par **Bootstrap** pour représenter ses icônes.

Enfin, le dossier `img` regroupe les quelques images utilisées sur l'interface web.

#### views

Les vues représentent les différentes pages de l'interface web et sont définies au format de templating **Go**, le
*gohtml* (ici l'extension *.tmpl*).

```
resources/views/
├── base.tmpl
├── error.tmpl
├── index.tmpl
├── statement.tmpl
├── view.tmpl
└── components
    ├── navbar.tmpl
    ├── notifications.tmpl
    └── small-profile.tmpl
```

Tout d'abord, on notera le fichier `base.tmpl` qui comme son nom l'indique sert de base aux autres vues, car il est le
seul à contenir les balises `<html>` et `<body>`, ce qui évite de les renseigner dans les autres vues.

Le dossier `components` référence quelques composants utilisés à plusieurs endroits, et donc déclarés dans un fichier à
part dans le but d'éviter les répétitions de code.

### users

Ce module dédié à la logique concernant les utilisateurs est assez léger, car le login sert uniquement de moyen
d'authentification.

```
users/
└── user.go
```

Le fichier `user.go` contient donc seulement un modèle d'utilisateur ainsi que ses méthodes.

### utils

Ce module regroupe des fonctions utilitaires importées dans tout le reste du projet. Il n'importe qu'un seul autre
package du projet : le logger.

```
utils/
├── map.go
├── map_test.go
├── math.go
├── math_test.go
├── misc.go
├── misc_test.go
├── set.go
├── set_test.go
├── slice.go
├── slice_test.go
├── template.go
└── template_test.go
```

Nous n'allons pas nous attarder sur chaque fichier, leur nom étant assez explicite. On remarquera tout de même la
présence d'un fichier de test par fichier **Go**, car il est très important de veiller à ce que les fonctions contenues
dans ces derniers soient les plus stables possible, en raison de l'étendue de leur utilisation dans l'ensemble du
projet.

Il est aussi plus simple de tester ces fonctions utilitaires car leurs paramètres et valeurs retournées sont le plus
souvent des types de base.

### web

Le module web constitue la partie *contrôleur* de l'application. C'est lui qui reçoit toutes les connexions des clients
et qui traite leurs demandes.

```
web/
├── auth.go
├── errors.go
├── index.go
├── renderer.go
├── statement.go
├── utils.go
├── view.go
├── authentication
└── respmodels
```

Le fichier `auth.go` contient les *handlers* de connexion, de déconnexion et de création de compte.

`errors.go` gère les différentes erreurs (404, 500, ...)

`index.go` s'occupe de la page d'accueil.

Le fichier `renderer.go` en revanche ne contient pas de contrôleur, mais une fonction permettant de gérer les
différentes templates *gohtml* afin de les assembler pour donner un résultat final à l'utilisateur.

Ensuite, `statement.go` rassemble les différents *handlers* relatifs aux formulaires de saisie des relevés.

`utils.go`, comme à son habitude, renferme des fonctions utiles au reste du module et notamment la représentation sous
forme d'objet de la navbar.

Pour finir, `view.go` se charge de l'affichage des tables des différents modèles.

#### authentication

```
web/authentication/
└── middlewares.go
```

Ce module contient un unique fichier rassemblant les différents middlewares d'authentification, c'est-à-dire les
fonctions appelées juste avant les contrôleurs afin d'effectuer des vérifications préliminaires sur les requêtes des
clients.

#### respmodels

respmodels contient différents objets servant au stockage des valeurs devant être transmises aux templates ou bien
directement au client en cas d'envoi de **Json**.

```
web/respmodels/
├── auth.go
├── common.go
├── error.go
├── nav.go
├── notif.go
├── statement.go
└── view.go
```

Encore une fois, les noms de certains fichiers étant assez explicites, voici seulement le détail de deux d'entre eux :

- `common.go` : l'ensemble des valeurs communes à toutes les pages
- `notif.go` : les données des notifications devant être affichées à l'utilisateur

## Conclusion

C'est donc grâce à tous ces modules que le projet fonctionne, en s'appuyant les uns sur les autres afin de former
ensemble une synergie, mais également en utilisant d'autres modules créés et maintenus par la communauté **Go**.
