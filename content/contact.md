---
draft: false
title: "Contact"
---

### Pour me joindre, vous pouvez m'envoyez :

- un mail à [thomas.delbende@ik.me](mailto:thomas.delbende@ik.me)
- un message via mon profil [LinkedIn](https://www.linkedin.com/in/thomas-delbende-109a9b1b4/)

### N'hésitez pas à explorer mes projets sur :

- [GitLab](https://gitlab.com/th_dl)
- [GitHub](https://github.com/Maxi-Mega)

### Mon CV :

{{< pdf path="CV_Thomas-Delbende_9.pdf" >}}
